package br.com.webzl;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"br.com.webzl.controller"})
public class MyConf {

}